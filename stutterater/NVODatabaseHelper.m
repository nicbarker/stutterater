//
//  NVODatabaseHelper.m
//  Notivo
//
//  Created by Nic Barker on 28/01/2014.
//  Copyright (c) 2014 Nic Barker. All rights reserved.
//
#import "NVODatabaseHelper.h"

@interface NVODatabaseHelper()

    @property NSManagedObjectContext * managedObjectContext;
    @property NSMutableDictionary * userObject;

@end

@implementation NVODatabaseHelper

/*
(NVODatabaseHelper *)getInstance
    This static method returns a singleton instance of the Database helper.
    If one doesn't already exist, it initializes and returns it.
*/
+ (NVODatabaseHelper *)getInstance {
    static NVODatabaseHelper * instance = nil; // The instance defaults to nil
    @synchronized(self) { // Encapsulate in a synchronized block to make initialization thread-safe, we don't want a race condition
        if (instance == nil) { // Allocate a new instance if there isn't one already
            instance = [[self alloc] init];
        }
    }
    return instance;
}

/*
(id)init
    Initializes a new instance of the object and returns it.
*/
- (id)init {
    self = [super init];
    // Get the managed object context and cache it locally so we don't have to make this horrible function call every time
    self.managedObjectContext = [((id)[[UIApplication sharedApplication] delegate]) managedObjectContext];
    return self;
}

/*
(NSFetchRequest *)getFetchRequestWithEntity:(NSString *)entity andPredicate:(NSPredicate *)predicate
    Constructs a fetch request that can be used to retrieve objects from the database.
    Purely a private method and should only be used internally.
 
    @entity: The name of the entity to retrieve. E.g "User"
    @predicate: The NSPredicate to apply to the fetch request. Similar to WHERE in SQL. NULL or nil is ok.
 
    @return: NSFetchRequest for the specified entity and predicate.
*/
- (NSFetchRequest *)getFetchRequestWithEntity:(NSString *)entity andPredicate:(NSPredicate *)predicate {
    // Create a fetch request
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    // Set up an entity to hold the database rows
    fetchRequest.entity = [NSEntityDescription entityForName:entity inManagedObjectContext:self.managedObjectContext];
    // Set the filter predicate
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    return fetchRequest;
}

/*
(NSMutableArray *)getAllDatabaseResultsMatchingEntity:(NSString *)entity sortOnColumn:(NSString *)column ascending:(BOOL)ascending resultsAsDictionary:(BOOL)resultsAsDictionary withPredicate:(NSPredicate *)predicate withFetchLimit:(NSInteger)fetchLimit
    
    A comprehensive method for retrieving data from the local database. Takes a range of params to manipulate the local select query.
 
    @entity: The name of the entity to retrieve. E.g "User"
    @column: The string name of the column to sort on.
    @ascending: A bool representing which way to sort. YES = ASC, NO = DESC. Only used if column is specified.
    @resultsAsDictionary: If YES, returns an NSArray of NSDictionary objects, rather than NSManagedObject objects.
    @predicate: The NSPredicate to apply to the fetch request. Similar to WHERE in SQL. NULL or nil is ok.
    @fetchLimit: The LIMIT clause in SQL - e.g how many rows to return.
 
    @return: An NSMutableArray of all the results returned: NSManagedObject(s) by default, or NSDictionaries if resultsAsDictionary is YES.
*/
- (NSMutableArray *)getAllDatabaseResultsMatchingEntity:(NSString *)entity sortOnColumn:(NSString *)column ascending:(BOOL)ascending resultsAsDictionary:(BOOL)resultsAsDictionary withPredicate:(NSPredicate *)predicate withFetchLimit:(NSInteger)fetchLimit {
    // First, get the appropriate fetch request
    NSFetchRequest * fetchRequest = [self getFetchRequestWithEntity:entity andPredicate:predicate];
    // If the user wants to sort the results, add a sort descriptor
    if (column != nil) {
        fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:column ascending:ascending], nil];
    }
    // If the user specified a fetch limit, limit the results
    if (fetchLimit > 0) {
        [fetchRequest setFetchLimit:fetchLimit];
    }
    NSError *error;
    // Execute the fetch request and catch any errors
    NSMutableArray * results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
    // Convert the results to dictionaries
    if (resultsAsDictionary) {
        for (int i = 0; i < results.count; i++) {
            results[i] = [NVOUtility createDictionaryFromNSManagedObject:results[i]];
        }
    }
    return results;
}

/*
(NSUInteger)countRowsForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate
    Returns an Integer count of the total number of rows that match the search query.
 
    @entity: The name of the entity to retrieve. E.g "User"
    @predicate: The NSPredicate to apply to the fetch request. Similar to WHERE in SQL. NULL or nil is ok.
 
    @return: Integer representing count of rows matching the search query. Returns 0 if none found.
*/
- (NSUInteger)countRowsForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate {
    // First, get the appropriate fetch request
    NSFetchRequest * fetchRequest = [self getFetchRequestWithEntity:entity andPredicate:predicate];
    [fetchRequest setIncludesSubentities:NO]; // Omit subentities, i.e don't do any joins. Just return ID.
    NSError *err;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest error:&err];
    if(count == NSNotFound) {
        return 0;
    }
    return count;
}

/*
(NSMutableDictionary *)getUserObject
    This is a convenience method for getting the user object from the database, rather than having to specify a million parameters.
 
    @return An NSMutableDictionary representation of the user object stored in the local db, or nil if there isn't one yet.
*/
- (NSMutableDictionary *)getUserObject {
    if ([self countRowsForEntity:@"User" withPredicate:nil] > 0) { // If there's a user object in the local db
        return self.userObject ? self.userObject : [[self getAllDatabaseResultsMatchingEntity:@"User" sortOnColumn:nil ascending:NO resultsAsDictionary:YES withPredicate:nil withFetchLimit:0] objectAtIndex:0]; // Retrieve the user object
    }
    return nil; // Else just return nil
}

/*
(NSMutableDictionary *)getRequestWithID:(NSNumber *)ID
    A convenience method to return a single event from the local database using it's request_id.
 
    @ID: the request_id of the event to return
 
    @return an NSMutableDictionary of the specified request object, or nil if there is no match.
*/
- (NSMutableDictionary *)getRequestWithID:(NSNumber *)ID {
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"request_id == %@", ID]; // Make a predicate to match the request id
    if ([self countRowsForEntity:@"Request" withPredicate:predicate] > 0) {
        return [self getAllDatabaseResultsMatchingEntity:@"Request" sortOnColumn:@"request_id" ascending:NO resultsAsDictionary:YES withPredicate:predicate withFetchLimit:1][0]; // Get the first match (there should only be one anyway)
    }
    return nil;
}

/*
(void)updateDatabaseObjectWithDictionary:(NSDictionary *)jsonObject withEntity:(NSString *)entity matchingOnColumn:(NSString *)column
    This method matches one more more entities from the local database and updates them with properties from the specified dictionary,
    matching keys to columns.
 
    @jsonObject: A dictionary containing key : value pairs that correspond to column : value pairs for the entity in the local db.
    @entity: The name of the entity to update. E.g "User"
    @column: The string name of the column to match on. This will be matched to the corresponding key in the jsonObject.
*/
- (void)updateDatabaseObjectWithDictionary:(NSDictionary *)jsonObject withEntity:(NSString *)entity matchingOnColumn:(NSString *)column {
    // First, search for an existing object and update if possible
    NSFetchRequest * fetchRequest = [self getFetchRequestWithEntity:entity andPredicate:nil];

    // If we're not updating the user object, apply a predicate to match on the input column
    if (![entity isEqualToString:@"User"]) {
        // Match on the appropriate column
        fetchRequest.predicate = [NSPredicate predicateWithFormat:[column stringByAppendingString:@" == %@"], jsonObject[@"column"]];
    }
    
    NSError * error = nil;
    NSArray * results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    // Use the retrieved object, or create a new one if none exists
    NSManagedObject * object = [results count] > 0 ? [results objectAtIndex:0] : [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:self.managedObjectContext];
    // Get the column names from the database table as a dictionary
    NSDictionary * attributes = [[NSEntityDescription entityForName:entity inManagedObjectContext:self.managedObjectContext] attributesByName];
    // Get the keys from that dictionary
    NSArray * attributeNames = attributes.allKeys;
    NSMutableDictionary * mutableJSON = [jsonObject mutableCopy];
    // Remove all the params that aren't strongly typed to the local database, to prevent database errors
    for (NSString * key in jsonObject.allKeys) {
        if (![attributeNames containsObject:key]) {
            [mutableJSON removeObjectForKey:key];
        }
    }
    // Update the database object and persist it
    [object setValuesForKeysWithDictionary:mutableJSON];
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error saving to local database: %@", [error localizedDescription]);
    }
}

/*
(void)resetLocalTableForEntity:(NSString *)entity
    Deletes all the rows in the specified table.
 
    @entity: The name of the entity to delete. E.g "Request"
*/
- (void)resetLocalTableForEntity:(NSString *)entity {
    NSFetchRequest * fetchRequest = [self getFetchRequestWithEntity:entity andPredicate:nil];
    [fetchRequest setIncludesPropertyValues:NO]; // only fetch the managedObjectID, not other attributes from the table to speed up the query
    
    NSError * error = nil;
    NSArray * requests = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    // Delete all the matching objects
    for (NSManagedObject * request in requests) {
        [self.managedObjectContext deleteObject:request];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
}

/*
(void)deleteRequestWithRequestId:(NSNumber *)requestId
     Deletes a row from the local table Request, matching on the specified request ID.
     
     @requestId: The request_id of the row to delete
*/
- (void)deleteRequestWithRequestId:(NSNumber *)requestId {
    NSFetchRequest * fetchRequest = [self getFetchRequestWithEntity:@"Request" andPredicate:[NSPredicate predicateWithFormat:@"request_id == %@", requestId]];
    [fetchRequest setIncludesPropertyValues:NO]; // only fetch the managedObjectID, not other attributes from the table
    
    NSError * error = nil;
    NSArray * requests = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([requests count] > 0) { // Make sure the request exists before trying to delete with it
        [self.managedObjectContext deleteObject:requests[0]];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"localDBChanged" object:self]; // Post a notification to alert of a change to the local db
}

/*
(void)updateAllEntities:(NSString *)entity withColumn:(NSString *)column withValue:(NSObject *)value
    Updates all the values of the specified column in the database with the specified value. Useful for doing things like
    setting all the tooltips to unread.
 
    @entity: The name of the entity to update. E.g "User"
    @column: The string name of the column to match on.
    @value: The value to set "column" to.
*/
- (void)updateAllEntities:(NSString *)entity withColumn:(NSString *)column withValue:(NSObject *)value {
    NSFetchRequest *fetchRequest = [self getFetchRequestWithEntity:entity andPredicate:nil];
    NSError *error;
    NSArray * results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [results setValue:value forKey:column];
    [self.managedObjectContext save:&error];
}

@end
