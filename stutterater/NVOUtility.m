//
//  NVOUtility.m
//  Notivo
//
//  Created by Nic Barker on 15/01/2014.
//  Copyright (c) 2014 Nic Barker. All rights reserved.
//

#import "NVOUtility.h"
#import <CoreData/CoreData.h>

@implementation NVOUtility

+ (NSDictionary *)getStatusDataFromText:(NSString *)text {
    NSDictionary *statusDictionary = [NVOUtility readPlistFileWithName:@"statusData"];
    if ([text isEqual: @"pending"]) {
        return [statusDictionary objectForKey:@"active"];
    }
    else if ([text isEqual: @"active"]) {
        return [statusDictionary objectForKey:@"active"];
    }
    else if ([text isEqual: @"completed"]) {
        return [statusDictionary objectForKey:@"completed"];
    }
    else if ([text isEqual: @"partial_failed"]) {
        return [statusDictionary objectForKey:@"completed"];
    }
    else if ([text isEqual: @"rejected"]) {
        return [statusDictionary objectForKey:@"rejected"];
    }
    else if ([text isEqual: @"clarifying"]) {
        return [statusDictionary objectForKey:@"clarifying"];
    }
    else {
        return 0;
    }
}

+ (UIColor *)createColorFromRgbaArray:(NSArray *)array {
    return [UIColor colorWithRed:[(NSNumber *)array[0] floatValue] green:[(NSNumber *)array[1] floatValue] blue:[(NSNumber *)array[2] floatValue] alpha:[(NSNumber *)array[3] floatValue]];
}

+ (NSMutableDictionary *)readPlistFileWithName:(NSString *)fileName {
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    return [[NSMutableDictionary alloc] initWithContentsOfFile:plistCatPath];
}

+ (NSMutableDictionary *)createDictionaryFromNSManagedObject:(NSManagedObject *)object {
    NSArray *keys = [[[object entity] attributesByName] allKeys];
    return [[object dictionaryWithValuesForKeys:keys] mutableCopy];
}

#pragma mark - Date Functions

+ (NSString *)suffixForDayInDate:(NSDate *)date {
    if (date != (id)[NSNull null]) {
        NSInteger day = [[[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] components:NSDayCalendarUnit fromDate:date] day];
        if (day >= 11 && day <= 13) {
            return @"th";
        } else if (day % 10 == 1) {
            return @"st";
        } else if (day % 10 == 2) {
            return @"nd";
        } else if (day % 10 == 3) {
            return @"rd";
        } else {
            return @"th";
        }
    }
    return @"";
}

+ (NSNumber * )daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [[NSNumber alloc] initWithLong:[difference day]];
}

+ (NSInteger)daysInThisMonth {
    NSDate *today = [NSDate date]; //Get a date object for today's date
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:today];
    return days.length;
}

+ (NSDate *)firstDayOfNextMonth {
    // Calculate the first day of next month
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = 1;
    NSDate *currentDatePlus1Month = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:currentDatePlus1Month];
    [comp setDay:1];
    NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
    return firstDayOfMonthDate;
}

#pragma mark - UI and animations

+ (void)fadeInUIView:(UIView *)view withDuration:(float)duration onComplete:(Callback)callback {
    view.alpha = 0;
    [UIView animateWithDuration:duration animations:^{
        view.alpha = 1;
    } completion:^(BOOL finished) {
        callback();
    }];
}

+ (void)fadeOutUIView:(UIView *)view withDuration:(float)duration onComplete:(Callback)callback {
    [UIView animateWithDuration:duration animations:^{
        view.alpha = 0;
    } completion:^(BOOL finished) {
        callback();
    }];
}

+ (void)fadeInUIActivityIndicatorView:(UIActivityIndicatorView *)view withDuration:(float)duration {
    [view startAnimating];
    [NVOUtility fadeInUIView:view withDuration:duration onComplete:^(){}];
}

+ (void)fadeOutUIActivityIndicatorView:(UIActivityIndicatorView *)view withDuration:(float)duration {
    [NVOUtility fadeOutUIView:view withDuration:duration onComplete:^() {
        [view stopAnimating];
    }];
}

+ (CABasicAnimation *)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    return rotationAnimation;
}

@end