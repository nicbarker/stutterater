//
//  NVOUtility.h
//  Notivo
//
//  Created by Nic Barker on 15/01/2014.
//  Copyright (c) 2014 Nic Barker. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ Callback)();

@interface NVOUtility : NSObject

// General Utility functions
+ (NSDictionary *)getStatusDataFromText:(NSString *)text;
+ (UIColor *)createColorFromRgbaArray:(NSArray *)dictionary;
+ (NSMutableDictionary *)readPlistFileWithName:(NSString *)fileName;

// Date functions
+ (NSString *)suffixForDayInDate:(NSDate *)date;
+ (NSNumber *)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSDate *)firstDayOfNextMonth;
+ (NSInteger)daysInThisMonth;

+ (void)getDataFromServerAndPopulateDatabase:(Callback)callBack shouldShowConnectionAlert:(BOOL)shouldShowConnectionAlert;

+ (void)fadeInUIView:(UIView *)view withDuration:(float)duration onComplete:(Callback)callback;
+ (void)fadeOutUIView:(UIView *)view withDuration:(float)duration onComplete:(Callback)callback;
+ (void)fadeInUIActivityIndicatorView:(UIActivityIndicatorView *)view withDuration:(float)duration;
+ (void)fadeOutUIActivityIndicatorView:(UIActivityIndicatorView *)view withDuration:(float)duration;

+ (CABasicAnimation *)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;

@end
