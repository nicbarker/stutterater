//
//  NVODatabaseHelper.h
//  Notivo
//
//  Created by Nic Barker on 28/01/2014.
//  Copyright (c) 2014 Nic Barker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "NVOUtility.h"

@interface NVODatabaseHelper : NSObject

+ (NVODatabaseHelper *)getInstance;

- (NSMutableArray *)getAllDatabaseResultsMatchingEntity:(NSString *)entity sortOnColumn:(NSString *)column ascending:(BOOL)ascending resultsAsDictionary:(BOOL)resultsAsDictionary withPredicate:(NSPredicate *)predicate withFetchLimit:(NSInteger)fetchLimit;

- (NSUInteger)countRowsForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate;

- (NSMutableDictionary *)getRequestWithID:(NSNumber *)ID;

- (NSMutableDictionary *)getUserObject;

- (void)updateDatabaseObjectWithDictionary:(NSDictionary *)jsonObject withEntity:(NSString *)entityName matchingOnColumn:(NSString *)column;

- (void)resetLocalTableForEntity:(NSString *)entity;

- (void)deleteRequestWithRequestId:(NSNumber *)requestId;

- (void)updateAllEntities:(NSString *)entity withColumn:(NSString *)column withValue:(NSObject *)value;

@end